# Let's create a new Person class that has a born year and died year
class Person
  attr_accessor :born, :died
  def initialize( born, died )
    @born = born
    @died = died
  end
end

# Create an array from the range of 1900 to 2000
years = (1900..2000).to_a

# Our array of people(of class Person)
people = [
  Person.new(1901, 1948),
  Person.new(1945, 1999),
  Person.new(1950, 1983),
  Person.new(1990, 1996),
  Person.new(1928, 1978),
  Person.new(1981, 1999),
  Person.new(1906, 1987),
  Person.new(1943, 1955),
  Person.new(1990, 2000),
  Person.new(1981, 1999)
]

# We use map to iterate over each year in the years array and then select the number of people alive during that year
alivePerYear = years.map{ |year| {"year" => year, "alive" => people.select{ |person| person.born <= year && person.died >= year }.length} }

 # we 'reduce' the alivePerYear to one hash with a year in which th most people were alive 
 mostAlive = alivePerYear.reduce({"year" => -1, "alive" => 0}) { |current, newYear|

  if newYear["alive"] > current["alive"]
    current = newYear
  end

  current
 }

 p mostAlive
